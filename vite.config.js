import { defineConfig } from 'vite'
import { resolve } from 'path'
import uservite from './vite' //引入vite插件文件

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: '0.0.0.0',
    port: 5173, // 端口
    proxy: {
      '/api': { // 请求接口中要替换的标识
        target: 'http://117.62.22.235:17009', // 代理地址
        changeOrigin: true, // 是否允许跨域
        ws: true, // 开启 websockets 代理
        secure: true, // 验证 SSL 证书
        rewrite: (path) => path.replace(/^\/api/, '') // api标志替换为''
      }
    }
  },
  plugins: uservite(),
  // 路径配置
  resolve: {
    alias: [
      {
        find: '@',
        replacement: resolve(__dirname, 'src'), //配置@ 路径
      },
      {
        find: 'assets',
        replacement: resolve(__dirname, 'src/assets'), //配置 assets 图片引入路径
      },
      {
        find: 'vue-i18n',
        replacement: 'vue-i18n/dist/vue-i18n.cjs.js', //配置 语言
      },
    ],
  }
})
