import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'

// 导出插件
export default function userViteImageOptimizer() {
  return ViteImageOptimizer({
    png: {
      // https://sharp.pixelplumbing.com/api-output#png
      quality: 90,
    },
    jpeg: {
      // https://sharp.pixelplumbing.com/api-output#jpeg
      quality: 90,
    },
    jpg: {
      // https://sharp.pixelplumbing.com/api-output#jpeg
      quality: 90,
    },
  })
}
