import Components from 'unplugin-vue-components/vite' //自动导入组件
import { ArcoResolver } from 'unplugin-vue-components/resolvers' //自动引入antdesign-vue组件
export default function userCompon() {
  // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
  return Components({
    // 指定组件位置，默认是src/components
    dirs: ['src/components'],
    // 配置文件生成位置
    dts: './src/components.d.ts',
    resolvers: [
      ArcoResolver({
        sideEffect: true,
      }),
    ],
  })
}
