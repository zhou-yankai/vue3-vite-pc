/*
 * @Description: 
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:13:45
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-01-30 17:26:42
 * @FilePath: viteAutoImport.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:13:45
 */
import AutoImport from 'unplugin-auto-import/vite' //自动导入库 比如res refs
import { ArcoResolver } from 'unplugin-vue-components/resolvers' //自动引入arcodesign-vue组件
export default function useratuoim() {
  // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
  return AutoImport({
    // Auto import functions from Vue, e.g. ref, reactive, toRef...
    // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
    imports: ['vue', 'vue-router'],
    // 可以选择auto-import.d.ts生成的位置，使用ts建议设置为'src/auto-import.d.ts'
    dts: './src/auto-import.d.ts',
    resolvers: [
      /* ... */
      ArcoResolver(),
    ],
    eslintrc: {
      enabled: false, // Default `false` //需要更新配置文件的时候在打开
      filepath: './.eslintrc-auto-import.json', // Default `./.eslintrc-auto-import.json`
      globalsPropValue: true, // Default `true`, (true | false | 'readonly' | 'readable' | 'writable' | 'writeable')
    },
  })
}
