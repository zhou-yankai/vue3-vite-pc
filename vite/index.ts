/*
 * @Description: vite插件
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:10:27
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-02-01 11:38:24
 * @FilePath: index.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:10:27
 */
import userCompression from './plugin/viteCompression' //gzip压缩插件
import useratuoim from './plugin/viteAutoImport' //自动导入库 比如res refs
import userCompon from './plugin/viteCompon' //自动导入组件
import userSvg from './plugin/viteSvg' //svg雪碧图
import userViteImageOptimizer from './plugin/viteImage'
import vue from '@vitejs/plugin-vue' //vue

// vite plug方法
export default function usevite() {
  return [
    vue(),
    userCompression(),
    useratuoim(),
    userSvg(),
    userCompon(),
    userViteImageOptimizer()
  ]
}

