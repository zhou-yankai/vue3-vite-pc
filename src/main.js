/*
 * @Description: main.js
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:02:30
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-01-31 09:31:13
 * @FilePath: main.js
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:02:30
 */
import { createApp } from 'vue'
import './style/style.css'
import '@arco-design/web-vue/dist/arco.css' //引入arcodesign 默认样式
import App from './App.vue'
import router from './router'
import pinia from './pinia' //引入pinia
import i18n from './locales' //引入语言包

import 'virtual:svg-icons-register' //引入svg雪碧图



const app = createApp(App)
app.use(router) // 使用路由
app.use(pinia) // 使用pinia状态管理器
app.use(i18n) // 使用i18n语言
app.mount('#app')
