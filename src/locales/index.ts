import { createI18n } from 'vue-i18n'

import zh from './zh/zh.js' //中文包
import en from './en/en.js' //英文包


const i18n = createI18n({
  locale: "zh", // 定义默认语言为中文
  messages: {
    zh,
    en,
  },
  // something vue-i18n options here ...
})


export default i18n