/*
 * @Description: 路由配置
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:28:18
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-02-04 13:57:46
 * @FilePath: index.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:28:18
 */
import { createRouter, createWebHashHistory } from "vue-router"
import layout from '@/layout/index.vue'
import NotFound from '@/layout/404/index.vue'

import createRouteGuard from './guard';

const routes = [
    // {
    //     path: '/',
    //     name: 'login',
    //     component: layout,
    //     children: [{
    //         path: '/logins',
    //         name: 'login',
    //         component: () => import('@/view/home/index.vue'),
    //         meta: { requiresAuth: false }
    //     }]
    // },
    {
        path: '/',
        component: layout,
        children: [{
            path: '',
            name: "首页",
            component: () => import('@/view/home/index.vue'),
            meta: { requiresAuth: false }
        }, {
            path: 'profiles',
            name: "详情",
            component: () => import('@/view/ce/index.vue'),
            meta: { requiresAuth: false }
        },
        {
            path: 'price',
            name: "价格",
            component: () => import('@/view/price/index.vue'),
            meta: { requiresAuth: false }
        },
        {
            path: 'about',
            name: "关于",
            component: () => import('@/view/about/index.vue'),
            meta: { requiresAuth: false }
        }]
    },
    {
        path: '/homes',
        name: 'homes',
        component: layout,
        children: [{
            path: 'profiless',
            component: () => import('@/view/home/index.vue'),
            meta: { requiresAuth: false }
        }]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/view/login/index.vue'),
    },
    {
        path: '/:pathMatch(.*)*',
        name: '啊哦，访问的页面被火星人劫走了...',
        component: NotFound
    },

]

export const router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
    scrollBehavior() {
        return { top: 0 };
    },
})


createRouteGuard(router)

export default router