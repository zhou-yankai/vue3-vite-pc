/*
 * @Description: 
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-02-01 11:52:59
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-02-01 11:53:48
 * @FilePath: index.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-02-01 11:52:59
 */
import type { Router } from 'vue-router';
// import { setRouteEmitter } from '@/utils/route-listener';
// import setupUserLoginInfoGuard from './userLoginInfo';
import setupPermissionGuard from './permission';

// function setupPageGuard(router: Router) {
//   router.beforeEach(async (to) => {
//     // emit route change
//     // setRouteEmitter(to);
//   });
// }

export default function createRouteGuard(router: Router) {
//   setupPageGuard(router);
//   setupUserLoginInfoGuard(router);
  setupPermissionGuard(router);
}
