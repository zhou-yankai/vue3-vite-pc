/*
 * @Description: 
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:36:58
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-01-30 17:37:14
 * @FilePath: index.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:36:58
 */
import { createPinia } from 'pinia'
// 使用持久化插件
import piniaPluginPersist from 'pinia-plugin-persist'
const pinia = createPinia()
pinia.use(piniaPluginPersist)
export default pinia //导出pinia

// 引入系统配置pinia
// import useAppStore from './moudel/App/app'
import userStore from './moudel/use'
// import userpermiss from './moudel/permiss/permisson'
// import usertabs from './moudel/permiss/tabs'
export {  userStore }
