import { defineStore } from 'pinia'

// import { setToken, removeToken } from '@/utils/cookie'
// import defAva from 'assets/logo.png' //默认图片
interface user {
  [x: string]: any
}

const userStore = defineStore('user', {
  // 为了完整类型推理，推荐使用箭头函数
  state: () => {
    return {
      // 所有这些属性都将自动推断出它们的类型
      count: 0,
      user: {} as user, //用户信息
      role: [], //权限
      avatar: '', //用户头像
      datas: {
        user: {
          id: 1,
          userName: 'admin',
          password: '',
          nickName: '万事皆胜意',
          userType: 1,
          headPortrait:
            '/upload/2022/11/23/f602c6e0-bed9-4433-be1f-4a11e5cd76fa.png',
          phoneNumber: '15819639008',
          email: 'ymnets@qq.com',
          address: '广东深圳',
          status: 1,
          remark: '啊啊啊啊',
          delFlag: 0,
          deptId: 10,
          createTime: null,
          createBy: 1,
          createName: '万事皆胜意',
          updateTime: 1669975390954,
          updateBy: 1,
          loginIp: '10.254.61.1',
          loginTime: 1675483093752,
          deptName: '总公司',
          roleList: [
            {
              id: 1,
              name: '超级管理员',
            },
          ],
          projectId: 100000,
          project: null,
        },
        token:
          'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NzU0OTI0MTAsImV4cCI6MTY3NjA5NzIxMCwiaWQiOjEsIm5hbWUiOiJhZG1pbiJ9.xsW160Uas49sWLJhTkSbzLX4SGBvo4OCLQjx-jCJaWo',
      },
    }
  },
  getters: {
    doubleCount: (state) => state.count * 2,
  },
  actions: {
    // 登录方法
    login(zhan: string, mima: any) {
      const moren = {
        admin: 'admin',
        mima: '123456',
      }
      return new Promise<void>((resolve, reject) => {
        if (zhan == moren.admin && mima == moren.mima) {
          // setToken(this.datas.token) //存储token
          resolve()
        } else {
          reject('账号或者密码错误')
        }
      })
    },
    info() {
      this.user = this.datas.user //设置用户信息
      // this.avatar =
      //   this.user.headPortrait == '' || this.user.headPortrait == null
      //     ? defAva
      //     : import.meta.env.VITE_APP_BASE_url + this.user.headPortrait
      this.role = [
        { code: 'system:add', name: '权限', id: '1' },
        { code: 'system:user', name: '权限', id: '1' },
      ] as any //设置用户权限
    },
    increment() {
      this.count++
    },
    randomizeCounter() {
      this.count = Math.round(100 * Math.random())
    },
    // 退出系统
    logout() {
      return new Promise((resolve) => {
        // logout(this.token)
        //   .then(() => {
        //     // 清除本地存储
        //     removeToken()
        //     this.role = []
        //     this.user = []
        //   })
        //   .catch((error) => {
        //     reject(error)
        //   })
        // 清除本地存储
        // removeToken()
        this.role = []
        this.user = []
        console.log('开始清除tokne了')
        resolve(true)
      })
    },
  },
})

export default userStore
