export default {
  '404.notFoundPage':
    'Exit tip Ah oh, the page visited was hijacked by the Martians...',
  '404.goHome': 'Go Home',
}
