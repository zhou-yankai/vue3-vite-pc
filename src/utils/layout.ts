/*
 * @Description: 
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-02-04 13:27:45
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-02-04 13:43:49
 * @FilePath: layout.ts
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-02-04 13:27:45
 */
// 网站标题
export function setTitle(title:string) {
    document.title = (title || '') + ' - ' + '高效的官网系统'
}