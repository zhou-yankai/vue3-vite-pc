/*
 * @Description: 
 * @Version: V1.0.0
 * @Author: 周艳凯 750419898@qq.com
 * @Date: 2024-01-30 17:45:51
 * @LastEditors: 周艳凯 750419898@qq.com
 * @LastEditTime: 2024-01-30 18:08:11
 * @FilePath: tailwind.config.js
 * Copyright 2024 Marvin, All Rights Reserved. 
 * 2024-01-30 17:45:51
 */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'false', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

